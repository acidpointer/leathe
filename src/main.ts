import {
  FastifyAdapter,
  NestFastifyApplication,
} from "@nestjs/platform-fastify";

import { NestFactory } from "@nestjs/core";
import { ConfigModule } from "@nestjs/config";
import http from "node:http";

import { AppModule } from "./modules/app.module.ts";

const bootstrap = async () => {
  const serverFactory = (handler, opts) => {
    const server = http.createServer((req, res) => {
      handler(req, res);
    });

    return server;
  };

  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter({
      logger: false,
      serverFactory,
    }),
  );

  await ConfigModule.envVariablesLoaded;

  await app.init();

  await app.listen(3000, "0.0.0.0");
};

bootstrap().catch((err) => {
  console.error("[BOOTSTRAP] Error:", err);
});
