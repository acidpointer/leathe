import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { DatabaseModule } from "./database.module.ts";

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      cache: true,
      envFilePath: [
        "./.env",
        "./.env.global",
        "./.env.prod",
      ],
    }),
    DatabaseModule,
  ],
})
export class AppModule {}
