#!/usr/bin/env bash

# Leathe run script
# Author: acidpointer aka 0xD5 (acidpointer@gmail.com)
# Licence: GPL-3.0


#BRANCH="$(git rev-parse --symbolic-full-name --abbrev-ref HEAD)"

SCRIPT="$(readlink -f "${BASH_SOURCE[0]}")"
DIR="$(dirname "$SCRIPT")"

function start () {
    deno task dev
}


start
